package com.example.haidar.learndatabinding.viewmodels;

import android.app.Dialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.haidar.learndatabinding.R;
import com.example.haidar.learndatabinding.models.MahasiswaModel;
import com.example.haidar.learndatabinding.views.MahasiswaActivity;

import static java.lang.Integer.valueOf;

public class ViewModelMahasiswa {
    public ObservableField<String> NIM = new ObservableField<>();
    public ObservableField<String> nama = new ObservableField<>();
    public ObservableField<Integer> semester = new ObservableField<>();

    private MahasiswaModel mhs;
    private Context context;


    public ViewModelMahasiswa(Context ctx){
        this.context= ctx;
        mhs = new MahasiswaModel("Haidar", "2345678", 8);
        this.NIM.set(mhs.NIM);
        this.nama.set(mhs.nama);
        this.semester.set(mhs.semester);
    }

    public void openDialog(View view){
        final Dialog d = new Dialog(context);
        d.setTitle("Ganti Data");
        d.setContentView(R.layout.dialog_gantidata);

        final Button btnSimpan = d.findViewById(R.id.btnSimpan);
        final Button btnBatal = d.findViewById(R.id.btnBatal);
        final EditText etNama = d.findViewById(R.id.etNama);
        final EditText etNIM = d.findViewById(R.id.etNIM);
        final EditText etSemester = d.findViewById(R.id.etSemester);


        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = etNama.getText().toString();
                String nim = etNIM.getText().toString();
                String semester = etSemester.getText().toString();

                gantiData(nama,nim, Integer.parseInt(semester));
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

        d.show();
    }
    public void gantiData(String nama, String nim, int semester){
        this.nama.set(nama);
        this.NIM.set(nim);
        this.semester.set(semester);
    }
}
