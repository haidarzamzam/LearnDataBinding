package com.example.haidar.learndatabinding.models;

public class MahasiswaModel {

    public String nama;
    public String NIM;
    public int semester;

    public MahasiswaModel(String nama, String NIM, int semester) {
        this.nama = nama;
        this.NIM = NIM;
        this.semester = semester;
    }
}
