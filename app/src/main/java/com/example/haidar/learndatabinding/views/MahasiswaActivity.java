package com.example.haidar.learndatabinding.views;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.haidar.learndatabinding.R;
import com.example.haidar.learndatabinding.databinding.ActivityMahasiswaBinding;
import com.example.haidar.learndatabinding.viewmodels.ViewModelMahasiswa;

public class MahasiswaActivity extends AppCompatActivity {

    private ActivityMahasiswaBinding mhsBinding;
    private ViewModelMahasiswa vmMahasiswa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mhsBinding = DataBindingUtil.setContentView(this, R.layout.activity_mahasiswa);
        vmMahasiswa = new ViewModelMahasiswa(this);
        mhsBinding.setMahasiswa(vmMahasiswa);
    }
}
